ppmrw: converts between the P3 and P6 image formats
author: Chance Nelson

NOTE: I ended up hitting a really difficult bug, and I was unable to finish the conversion methods. What is currently done with the project:
-Lots of command line error checking
-copying files (as in making a P3 conversion of a P3 image, etc.)

command usage: ppmrw 3/6 in.ppm out.ppm