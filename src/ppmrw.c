#include <stdio.h>      // For print(), fopen(), etc.
#include <stdlib.h>     // For NULL and all your other basic C needs
#include <stdbool.h>    // For boolean operations
#include <string.h>     // For strstr()
#include <unistd.h>     // for access()
#include <ctype.h>      
#include <string.h>     // for strcmp()

// Forward declaring methods
char getPPMType(char*);
bool checkArgs(int, char**);
void copyFile(FILE*, FILE*);
void threeToSix(FILE*, FILE*, char**);
void sixToThree(FILE*, FILE*, char**);
char* toBinary(int);

// Checks for correct args and format
// Returns false if command is not passed to the correct specification:
// ppmrw 3/6 in.ppm out.ppm
bool checkArgs(int argc, char** argv) {
    if(argc < 4) return false;          // Should be 4 args including filename

    // Make sure all the files have the .ppm extension
    const char* ppm = ".ppm";           
    if((strstr(argv[2], ppm) == NULL) ||  (strstr(argv[3], ppm) == NULL)) return false;
 
    // Check for input file existence
    if((access(argv[2], R_OK) == -1)) {
        printf("Error: input file %s does not exist\n", argv[2]);
        return false;                   
    }

    // Make sure the input file is of the legal types (p3 or p6)
    char * in = argv[2];
    char p3 = '3';
    char p6 = '6';
    if((getPPMType(in) !=  p3) && (getPPMType(in) != p6)) {
            printf("Error: input file %s is not of format P3 or P6\n", argv[2]);
            return false;
        }

    return true;                        // Everything looks good

}

// Gets the type of the PPM file in question
// Returns: string extracted from the first line of the input file
char getPPMType(char* fileName) {
    char fileType;
    FILE *file = fopen(fileName, "r");

    fileType = fgetc(file);
    fileType = fgetc(file);
    return fileType;
}

char* toBinary(int num) {
    char* bin = malloc(1);
    int count = 0;
    while(num > 0) {
        bin[count] = num % 2;
        num = num / 2;
    }

    return bin;
}

void copyFile(FILE* in, FILE* out) {
    rewind(in);
    rewind(out);

    char buffer;
    while(feof(in) == 0) {
        buffer = fgetc(in);
        fputc(buffer, out);
    }

    return;

}

void threeToSix(FILE* in, FILE* out, char** dimensions) {
    return;
    
    rewind(in);
    rewind(out);

    char buffer[10000];

    int i = 0;
    for(i; i < 3; i++) {
        fgets(buffer, 10000, in);
    }

    fputs("P6\n", out);
    fputs(dimensions[0], out);
    fputs(dimensions[1], out);
    fputs(dimensions[2], out);
    fputs("\n", out);
    fputs("255\n", out);

    while(feof(in) == 0) {
        fgets(buffer, 10000, in);
        char* temp;
        temp = strtok(buffer, " ");
        
    }

    return;

}

void sixToThree(FILE* in, FILE* out, char** dimensions) {
    return;
}

int main(int argc, char** argv) {
    if(checkArgs(argc, argv) == false){ // Check command line args
        printf("ppmrw: convert ppm formats p3 and p6\ncorrect usage: ppmrw 3/6 in.ppm out.ppm");
        return 0;
    }

    FILE* in = fopen(argv[2], "r");     // Opening the files so they can be passed on accordingly
    FILE* out = fopen(argv[3], "w");

     // Set up dimentions to hold the final size, and a temp buffer for file reading
    char* dimensions[3];
    char buffer[64];
    
    rewind(in);     // Reset file pointer to beginning

    // Get new lines until we are at the line that hold the dimensions information
    fgets(buffer, 8, in);
    while((buffer[0] == 'P') || (buffer[0] == '#')) fgets(buffer, 64, in);
    
    char* temp;

    temp = strtok(buffer, " ");
    char* dimX = temp;

    temp = strtok(buffer, " ");
    char* dimY = temp;
    
    dimensions[0] = dimX;
    dimensions[1] = " ";
    dimensions[2] = dimY;

    rewind(in);     // Reset file pointer

    // Figure out what sort of cnversion (or copy) is going on and pass to the correct function
    char convertFormat = argv[1][0];
    
    if(convertFormat == getPPMType(argv[2])) {
        copyFile(in, out);
        return 0;

    }

    if((convertFormat == '6') && (getPPMType(argv[3]) == '3')) {
        sixToThree(in, out, dimensions);
        return 0;

    } else {
        threeToSix(in, out, dimensions);
        return 0;

    }    

    return 0;       // Exit

}
