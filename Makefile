# simple makefile for Project 1
# @author: Chance Nelson

# Adding some simple flags so I may reuse this later with less stress
CC = gcc
ODIR = obj
SDIR = src

global:
	$(CC) $(SDIR)/*.c			# Calls gcc and compiles everything in the src directory
	mkdir $(ODIR)				# Make directroy 'obj' if it doesnt already exist
	mv *.out $(ODIR)/

clean:
	rm $(ODIR)/*.out
	rm -rf $(ODIR)
